package org.chevron.examples;

import freemarker.template.Configuration;
import org.chevron.DefaultChevronContext;
import org.chevron.core.Composer;
import org.chevron.core.composition.CompositionParams;
import org.chevron.core.util.KeyValueObject;
import org.chevron.freemaker.FreemakerEngine;
import org.chevron.json.JsonSchemaPool;
import org.chevron.json.source.ClasspathSchemaSource;
import org.chevron.model.Format;
import org.chevron.model.ObjectSchemasPool;
import org.chevron.model.Qualifiers;
import org.chevron.model.TemplatesPool;
import org.chevron.templating.DefaultTemplatesPool;
import org.chevron.templating.loader.ClasspathTemplateLoader;

import java.util.Collections;


public class Example1 {

    public static void main(String[] args){

        ObjectSchemasPool objectSchemasPool = JsonSchemaPool.builder()
                .enableCaching()
                .enlist(new ClasspathSchemaSource("META-INF/examples/1/schemas"))
                .build();

        TemplatesPool templatesPool = DefaultTemplatesPool.builder()
                .enableCaching()
                .enlist(new ClasspathTemplateLoader("META-INF/examples/1/templates"))
                .build();

        DefaultChevronContext context = DefaultChevronContext.builder()
                .register(new FreemakerEngine(),new Configuration())
                .objectSchemasPool(objectSchemasPool)
                .templatesPool(templatesPool)
                .define("systemName","Chevron")
                .define("systemVersion","0.1.0")
                .define("developer",KeyValueObject.builder()
                            .put("email","john.doe@gmail.com")
                            .put("badge","beginner")
                            .build())
                .build();


        CompositionParams params = CompositionParams.builder()
                .put("person", KeyValueObject.builder()
                        .put("firstName","Mario")
                        .put("lastName","Junior")
                        .build())
                .build();

        Composer composer = new Composer(context);
        String output = composer.compose("Example", Format.HTML,
                Collections.singletonList(Qualifiers.EN),
                params);

        System.out.println(output);

    }

}
